"use strict";

jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.a-home-seo__more').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('a-home-seo__more_active')) {
      $(this).removeClass('a-home-seo__more_active');
      $(this).html('Читать дальше');
      $(this).prev().removeClass('a-home-seo__content_active');
    } else {
      $(this).addClass('a-home-seo__more_active');
      $(this).html('Скрыть');
      $(this).prev().addClass('a-home-seo__content_active');
    }
  }); // Iterate over each select element

  $('.custom-select').each(function () {
    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length; // Hides the select element

    $this.addClass('s-hidden'); // Wrap the select element in a div

    $this.wrap('<div class="select"></div>'); // Insert a styled div to sit over the top of the hidden select element

    $this.after('<div class="styledSelect"></div>'); // Cache the styled div

    var $styledSelect = $this.next('div.styledSelect'); // Show the first select option in the styled div

    $styledSelect.text($this.children('option').eq(0).text()); // Insert an unordered list after the styled div and also cache the list

    var $list = $('<ul />', {
      class: 'options'
    }).insertAfter($styledSelect); // Insert a list item into the unordered list for each select option

    for (var i = 0; i < numberOfOptions; i++) {
      $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
      }).appendTo($list);
    } // Cache the list items


    var $listItems = $list.children('li'); // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)

    $styledSelect.click(function (e) {
      e.stopPropagation();
      $('div.styledSelect.active').each(function () {
        $(this).removeClass('active').next('ul.options').hide();
      });
      $(this).toggleClass('active').next('ul.options').toggle();
    }); // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option

    $listItems.click(function (e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $this.val($(this).attr('rel'));
      $list.hide();
      /* alert($this.val()); Uncomment this for demonstration! */
    }); // Hides the unordered list when clicking outside of it

    $(document).click(function () {
      $styledSelect.removeClass('active');
      $list.hide();
    });
  });

  window.onscroll = function () {
    scrollFunction();
  };

  function scrollFunction() {
    if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
      $('#up').addClass('active');
    } else {
      $('#up').removeClass('active');
    }
  }

  new Swiper('.a-home-works__cards', {
    navigation: {
      nextEl: '.a-home-works .swiper-button-next',
      prevEl: '.a-home-works .swiper-button-prev'
    },
    loop: true,
    spaceBetween: 110,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper('.a-home-reviews__cards', {
    navigation: {
      nextEl: '.a-home-reviews .swiper-button-next',
      prevEl: '.a-home-reviews .swiper-button-prev'
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  $(window).scroll(function () {
    var top_of_element = $('#progress').offset().top;
    var bottom_of_element = $('#progress').offset().top + $('#progress').outerHeight();
    var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    var top_of_screen = $(window).scrollTop();

    if (bottom_of_screen > top_of_element && top_of_screen < bottom_of_element) {
      bar.animate(0.85);
    } else {// the element is not visible, do something else
    }
  });
  /* $(window).on("load resize", function () {
    if ($(window).width() < 1200) {
      $(".a-header__toggle").on("click", function (e) {
        e.preventDefault();
          $(this).toggleClass("a-header__toggle_active");
        $(".a-header").toggleClass("a-header_active");
        $(".a-header__drop").slideToggle("fast");
        $(".a-header__row").slideToggle("fast");
        $(".a-header__button").slideToggle("fast");
      });
    } else {
      $(".a-header__toggle").on("click", function (e) {
        e.preventDefault();
          $(this).toggleClass("a-header__toggle_active");
        $(".a-header").toggleClass("a-header_active");
        $(".a-header__drop").slideToggle("fast");
      });
    }
  }); */

  $('.a-header__toggle').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('a-header__toggle_active');
    $('.a-header').toggleClass('a-header_active');
    $('.a-header__sub').slideToggle('fast');
    $('.a-header__social').slideToggle('fast');
    $('.a-header__row').slideToggle('fast');
    $('.a-header__button').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').toggle();
  });
  $('.a-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'a-modal__centered') {
      $('.a-modal').hide();
    }
  });
  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').hide();
  });
  new WOW().init();
});
//# sourceMappingURL=main.js.map